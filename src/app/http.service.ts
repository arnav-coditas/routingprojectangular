import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'any'
})
export class HttpService {
 

  constructor(private http: HttpClient) { }

  getuserList() {
    return this.http.get("https://jsonplaceholder.typicode.com/users")
 
  }

  getDetails(id:any){
    return this.http.get(`https://jsonplaceholder.typicode.com/users/${id}`)
  }

}
