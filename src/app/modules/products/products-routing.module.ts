import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './product-home/details/details.component';
import { ProductHomeComponent } from './product-home/product-home.component';
import { StockComponent } from './product-home/stock/stock.component';


const routes: Routes = [
  { path: '', component: ProductHomeComponent, children: [
    
        { path: 'stock', component: StockComponent },
        { path: 'details', component: DetailsComponent },
   
  
  ],
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})
export class ProductsRoutingModule { }
