import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductHomeComponent } from './product-home/product-home.component';
import { HttpClientModule} from '@angular/common/http';
import { StockComponent } from './product-home/stock/stock.component';
import { DetailsComponent } from './product-home/details/details.component';

@NgModule({
  declarations: [
    ProductHomeComponent,
    StockComponent,
    DetailsComponent,
  
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    HttpClientModule,
    
  ]
})
export class ProductsModule { }
