import { outputAst } from '@angular/compiler';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';
import { IData } from 'src/app/IData/IData';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {
userlist!:any;

userdata!:IData
@Output() senddata = new EventEmitter()
  constructor(private DataService: HttpService, private router: Router ) { }

  ngOnInit(): void {
       this.DataService.getuserList().subscribe({
      next:(Response:any)=>{
     console.log(Response)
     this.userlist=Response

      }
      
      
    })
  }


  showDetails(data:any){
    this.senddata.emit(data)
    this.userdata=data
    
    console.log(this.userdata)
    // const link = ['/name' ];
    // this.router.navigate(link);
  }
}
