import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from 'src/app/http.service';
import { IData } from 'src/app/IData/IData';

@Component({
  selector: 'app-display-name',
  templateUrl: './display-name.component.html',
  styleUrls: ['./display-name.component.scss']
})
export class DisplayNameComponent implements OnInit {
  id:number=0
  userdetails!:any
 constructor(private route: ActivatedRoute,private DataService: HttpService){}
  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id=params['id'];
      });
      
  
  this.DataService.getDetails(this.id).subscribe({
    next:(Response:any)=>{
   console.log(Response)
   this.userdetails=Response
    
     
    }})

}
}
