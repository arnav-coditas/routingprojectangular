import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-display-company',
  templateUrl: './display-company.component.html',
  styleUrls: ['./display-company.component.scss']
})
export class DisplayCompanyComponent implements OnInit {
  id:any
  userdetails:any
  constructor(private route: ActivatedRoute,private DataService: HttpService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id=params['id'];
      });
      
  
  this.DataService.getDetails(this.id).subscribe({
    next:(Response:any)=>{
   console.log(Response)
   this.userdetails=Response
    
     
    }})
  }
  

}
