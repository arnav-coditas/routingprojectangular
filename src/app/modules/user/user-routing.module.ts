import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from 'src/app/guard/user.guard';
import { UserHomeComponent } from './user-home/user-home.component';
import { DisplayCompanyComponent } from './user-home/user-list/display-company/display-company.component';
import { DisplayNameComponent } from './user-home/user-list/display-name/display-name.component';
import { UserListComponent } from './user-home/user-list/user-list.component';

const routes: Routes = [
  {
    path: '',
    component: UserHomeComponent,
    children: [
      {
        path: ':id',
        component: UserListComponent,
        children: [
          { path: 'name/:id', component: DisplayNameComponent },
          { path: 'company/:id', component: DisplayCompanyComponent },
        ],
      },
    ],
  },

  //  {
  //   path:'',component: UserListComponent,children:[{path:'name',component:DisplayNameComponent}]
  //  },
  //  {
  //   path:'',component: UserListComponent,children:[{path:'/company',component:DisplayCompanyComponent}]
  //  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
 
})
export class UserRoutingModule { }

