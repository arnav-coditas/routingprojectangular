import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserListComponent } from './user-home/user-list/user-list.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DisplayNameComponent } from './user-home/user-list/display-name/display-name.component';
import { DisplayCompanyComponent } from './user-home/user-list/display-company/display-company.component';
import { NO_ERRORS_SCHEMA } from '@angular/compiler';




@NgModule({
  declarations: [
    UserHomeComponent,
    UserListComponent,
    DisplayNameComponent,
    DisplayCompanyComponent,
    
 
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    HttpClientModule
  
   
  ],

})
export class UserModule { }
