import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductGuard } from './guard/product.guard';
import { UserGuard } from './guard/user.guard';

const routes: Routes = [
  {
    path: 'users', loadChildren: ()=> import('./modules/user/user.module').then(m=>m.UserModule),
    canActivate:[UserGuard]
  },
  {
    path: 'products', loadChildren: ()=> import('./modules/products/products.module').then(n=>n.ProductsModule),
    canActivate:[ProductGuard]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
